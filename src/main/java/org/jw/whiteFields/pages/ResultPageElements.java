package org.jw.whiteFields.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ResultPageElements {

    @FindBy(how = How.XPATH,  using = "/html/body/div/div[3]/div/div/div[2]")
    public WebElement firstResult;

    @FindBy(how = How.XPATH, using = "/html/body/div/div[3]/div/div/div[3]")
    public WebElement secondResult;

    @FindBy(how = How.XPATH, using = "/html/body/div/div[3]/div/div/div[4]")
    public WebElement thirdResult;

    @FindBy(how = How.XPATH, using = "/html/body/div/div[3]/div/div/div[5]")
    public WebElement fourthResult;

    @FindBy(how = How.XPATH, using = "/html/body/div/div[3]/div/div/div[6]")
    public WebElement fifthResult;

    @FindBy(how = How.XPATH, using = "/html/body/div/div[3]/div/div/div/ul/li")
    public WebElement resultsFound;

    @FindBy(how = How.ID, using = "contact-form")
    public WebElement contactForm;
}
