package org.jw.whiteFields.pages;

import org.jw.whiteFields.Selenium.BrowserDriver;
import org.jw.whiteFields.core.Details;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ResultPage {

    ResultPageElements boundDataContainer = PageFactory.initElements(BrowserDriver.getCurrentDriver(), ResultPageElements.class);
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultPage.class);
    public List<Details> getDetails(String previousUrl) {
        List<Details> details = new ArrayList<Details>();
        retry(previousUrl);
        try {
            if (boundDataContainer.resultsFound.getText().equalsIgnoreCase("Your search returned no results.")) {
                return details;
            }
        } catch (Exception e) {
            retry(previousUrl);
        }
        try {
            tryAllElementsInPage(details);

        } catch (NoSuchElementException nse) {
            LOGGER.info("element not found");
        }
        return details;
    }

    private void tryAllElementsInPage(List<Details> details) {
        WebElement result = boundDataContainer.firstResult;
        Details detailObj = getDetailObject(result);
        if (detailObj != null) {
            details.add(detailObj);
            result = boundDataContainer.secondResult;
            detailObj = getDetailObject(result);
            if (detailObj != null) {
                details.add(detailObj);
                result = boundDataContainer.thirdResult;
                detailObj = getDetailObject(result);
                if (detailObj != null) {
                    details.add(detailObj);
                    result = boundDataContainer.fourthResult;
                    detailObj = getDetailObject(result);
                    if (detailObj != null) {
                        details.add(detailObj);
                        result = boundDataContainer.fifthResult;
                        detailObj = getDetailObject(result);
                        if (detailObj != null) {
                            details.add(detailObj);
                        }
                    }
                }
            }
        }
    }

    private void retry(String previousUrl) {
        try {

            WebElement contactForm = BrowserDriver.findElement(By.id("contact-form"));

            if (contactForm != BrowserDriver.NULL_WEB_ELEMENT) {
                BrowserDriver.pauseSearchFor(120000);
                BrowserDriver.loadPage(previousUrl);

            }
        } catch (NoSuchElementException nse) {
            LOGGER.info("Continue browsing to " + previousUrl);
        }
    }

    public Details getDetailObject(WebElement element) {
        try {
            Details detailObj = new Details();
            if (!element.getText().contains("<< ... Previous 12345678910 Next ... >>")) {
                String s = element.getText().replaceAll("Street Map","");
                s = s.replaceAll("More on CDRom","");
                detailObj.setDetailsDivText(s);
                return detailObj;
            }
            return null;
        } catch (NoSuchElementException nse) {
            LOGGER.info("element not found");
            return null;
        }
    }
}
