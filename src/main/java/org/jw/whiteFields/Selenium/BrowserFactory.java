package org.jw.whiteFields.Selenium;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;


public class BrowserFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(BrowserFactory.class);

    public static WebDriver getBrowser() {
        LOGGER.info("getBrowser()");
        WebDriver mBrowser;
        String browserType = System.getProperty("browser");
        if (browserType != null) {
            if (browserType.equalsIgnoreCase("firefox")) {
                mBrowser = getFirefoxBrowser("12");
            } else if (browserType.equalsIgnoreCase("firefox7")) {
                mBrowser = getFirefoxBrowser("7");
            } else if (browserType.equalsIgnoreCase("ie8")) {
                mBrowser = getIEDriver("8");
            } else if (browserType.equalsIgnoreCase("ie6")) {
                mBrowser = getIEDriver("6");
            } else if (browserType.equalsIgnoreCase("ios")) {
                mBrowser = getIOSDriver();
            } else if (browserType.equalsIgnoreCase("android")) {
                mBrowser = getAndroidDriver();
            } else if (browserType.equalsIgnoreCase("chrome")) {
                mBrowser = getChromeDriver();
            } else if (browserType.equalsIgnoreCase("safari")) {
                mBrowser = getSafariDriver();
            } else if (browserType.equalsIgnoreCase("htmlunit")) {
                mBrowser = new HtmlUnitDriver();
            } else {
                mBrowser = getFirefoxBrowser("12");
            }
        } else {
            LOGGER.info("No browser type specified. Defaulting to using a remote firefox browser");
            System.setProperty("remote", "true");
            mBrowser = getFirefoxBrowser("12");
        }
        return mBrowser;
    }


    public static WebDriver getFirefoxBrowser(String version) {
        LOGGER.info("getFirefoxBrowser()");
        WebDriver mBrowser;

        try {
                LOGGER.info("Getting local firefox web driver");
                mBrowser = new FirefoxDriver();
                mBrowser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        } catch (Exception e) {
            LOGGER.info("Error with firefox driver initialisation.");
            LOGGER.info(e.getMessage());
            return null;
        }
        return mBrowser;
    }


    private static WebDriver getChromeDriver() {
        LOGGER.info("getChromeDriver()");
        WebDriver mBrowser;
        String remoteValue = System.getProperty("remote");
        try {

            if (remoteValue != null && remoteValue.equalsIgnoreCase("true")) {
                LOGGER.info("Getting remote chrome web driver");
                DesiredCapabilities chrome = DesiredCapabilities.chrome();
                chrome.setCapability("chrome.switches", Arrays.asList("--start-maximized"));
                mBrowser = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), chrome);
            } else {
				System.setProperty("webdriver.chrome.driver", "chrome//chromedriver.exe");
                LOGGER.info("Getting local chrome web driver");
                mBrowser = new ChromeDriver();
            }

            mBrowser.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        } catch (Exception e) {
            LOGGER.info("Error with chrome driver initialisation. " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        return mBrowser;
    }

    private static WebDriver getAndroidDriver() {
        LOGGER.info("getAndroidDriver()");
        WebDriver mBrowser;
        try {
            String remoteValue = System.getProperty("remote");
            if (remoteValue != null && remoteValue.equalsIgnoreCase("true")) {
                LOGGER.info("Getting remote android web driver");
                mBrowser = new RemoteWebDriver(new URL("http://192.168.180.31:8081/wd/hub"), DesiredCapabilities.android());
            } else {
                LOGGER.info("Getting local android web driver");
                mBrowser = new RemoteWebDriver(new URL("http://localhost:8080/wd/hub"), DesiredCapabilities.android());
            }
        } catch (Exception e) {
            LOGGER.info("Error with android driver initialisation. " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        return mBrowser;
    }

    private static WebDriver getIOSDriver() {
        LOGGER.info("getIOSBrowser()");
        WebDriver mBrowser;

        try {
            //mBrowser =  new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.iphone());
            mBrowser = new RemoteWebDriver(new URL("http://192.168.180.35:3001/wd/hub"), new DesiredCapabilities("iPhone", "1", Platform.MAC));
            mBrowser.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        } catch (Exception e) {
            LOGGER.info("Error with IOS driver initialisation. " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        return mBrowser;
    }


    private static WebDriver getIEDriver(String version) {
        LOGGER.info("getIEBrowser()");
        WebDriver mBrowser;

        try {
            String remoteValue = System.getProperty("remote");
            DesiredCapabilities ie = DesiredCapabilities.internetExplorer();
            ie.setPlatform(Platform.ANY);

            if (remoteValue != null && remoteValue.equalsIgnoreCase("true")) {
                ie.setVersion(version);
                LOGGER.info("Getting remote IE" + version + " web driver");
                mBrowser = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), ie);
            } else {
                LOGGER.info("Getting local IE web driver");
                mBrowser = new InternetExplorerDriver(ie);
            }
            mBrowser.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            //mBrowser.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

        } catch (Exception e) {
            LOGGER.info("Error with ie initialisation. Version " + version);
            LOGGER.info(e.getMessage());
            return null;
        }
        return mBrowser;
    }

    private static WebDriver getSafariDriver() {
        LOGGER.info("getSafariDriver()");
        WebDriver mBrowser;

        try {
            String remoteValue = System.getProperty("remote");

            if (remoteValue != null && remoteValue.equalsIgnoreCase("true")) {
                LOGGER.info("Getting remote safari web driver");
                mBrowser = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.safari());
            } else {
                LOGGER.info("Getting local safari web driver");
                mBrowser = new SafariDriver();
            }
            mBrowser.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            //mBrowser.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

        } catch (Exception e) {
            LOGGER.info("Error with safari initialisation.");
            LOGGER.info(e.getMessage());
            return null;
        }
        return mBrowser;
    }


}
