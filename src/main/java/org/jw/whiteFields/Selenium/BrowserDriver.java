package org.jw.whiteFields.Selenium;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * @author GB046652
 */
public class BrowserDriver {

    private static final Logger LOGGER = LoggerFactory.getLogger(BrowserDriver.class);
    public static final WebElement NULL_WEB_ELEMENT = new RemoteWebElement();
    private static final List<WebElement> NULL_WEB_ELEMENT_LIST = new ArrayList<WebElement>();
    private static final String NULL_STRING = "NULL";

    public static int WAIT_SECONDS = 20;
    public static final String className = "BrowserDriver";
    private static WebDriver mDriver;

    //Search methods

    public static WebElement findElement(By by) {
        reloadBrowser();
        return get(by);
    }

    public static WebElement findElement(String id) {
        return findElement(By.id(id));
    }

    public static List<WebElement> findElements(By by) {
        reloadBrowser();
        return getListWithWait(by);
    }


    public static WebElement findElementFromList(By by, int index) {
        return findElements(by).get(index);
    }

    public static List<WebElement> findElements(By mainElementBy, By subElementsBy) {
        WebElement main = findElement(mainElementBy);
        return main.equals(NULL_WEB_ELEMENT) ? NULL_WEB_ELEMENT_LIST : main.findElements(subElementsBy);
    }

    public static WebElement findElement(By mainElementBy, By subElementsBy) {
        WebElement main = findElement(mainElementBy);
        return main.equals(NULL_WEB_ELEMENT) ? NULL_WEB_ELEMENT : main.findElement(subElementsBy);
    }

    //Web Element action methods

    public static void click(By by) {
        WebElement element = findElement(by);
        if (element == NULL_WEB_ELEMENT || element == null) {
            LOGGER.info("Element with By: " + by + " wasn't clicked!");
        } else {
            LOGGER.info("element is not null");
            element.click();
        }
    }

    public static void click(String id) {
        click(By.id(id));
    }

    public static void click(WebElement webElement) {
        if (webElement != null) webElement.click();
    }

    public static void clear(By by) {
        WebElement element = findElement(by);
        if (element != NULL_WEB_ELEMENT) element.clear();
    }

    public static void clear(String id) {
        clear(By.id(id));
    }

    public static void clear(WebElement element) {
        if (element != null) element.clear();
    }

    public static void sendKeys(By by, String value) {
        WebElement element = findElement(by);
        if (element != NULL_WEB_ELEMENT) element.sendKeys(value);
    }

    public static void sendKeys(String id, String value) {
        sendKeys(By.id(id), value);
    }

    public static void sendKeys(WebElement element, String value) {
        if (element != null) element.sendKeys(value);
    }

    public static String getElementText(By by) {
        WebElement element = findElement(by);
        if (element != NULL_WEB_ELEMENT) return element.getText();
        return NULL_STRING;
    }

    public static String getElementText(String id) {
        return getElementText(By.id(id));
    }

    public static String getAttribute(String id, String attribute) {
        return getAttribute(By.id(id), attribute);
    }

    public static String getAttribute(By by, String attribute) {
        WebElement element = findElement(by);
        return element.equals(NULL_WEB_ELEMENT) ? NULL_STRING : element.getAttribute(attribute);
    }

    public static String getAttribute(WebElement element, String attribute) {
        if (element == null) {
            return NULL_STRING;
        }
        LOGGER.info("WebElement " + attribute + " value: " + element.getAttribute(attribute));
        return element.getAttribute(attribute) == (null) ? NULL_STRING : element.getAttribute(attribute);

    }

    public static boolean verifyAttributeValue(By by, String attribute, String expected) {
        return getAttribute(by, attribute).equals(expected);
    }

    public static boolean verifyAttributeValue(String id, String attribute, String expected) {
        return getAttribute(id, attribute).equals(expected);
    }

    public static boolean verifyAttributeValue(WebElement webElement, String attribute, String expected) {
        return getAttribute(webElement, attribute).equals(expected);
    }

    public static String getCssValue(By by, String cssValue) {
        WebElement element = findElement(by);
        return element.equals(NULL_WEB_ELEMENT) ? NULL_STRING : element.getCssValue(cssValue);
    }

    public static String getCssValue(String id, String cssValue) {
        return getCssValue(By.id(id), cssValue);
    }

    public static String getTagName(By by) {
        WebElement element = findElement(by);
        return element.equals(NULL_WEB_ELEMENT) ? NULL_STRING : element.getTagName();
    }

    public static String getTagName(String id) {
        return getTagName(By.id(id));
    }

    public static boolean verifyElementText(By by, String expected) {
        return getElementText(by).equals(expected);
    }

    public static boolean verifyElementText(String id, String expected) {
        return verifyElementText(By.id(id), expected);
    }

    public static List<WebElement> getDropDownOptions(WebElement webElement) {
        Select select = new Select(webElement);
        return select.getOptions();
    }

    public static WebElement getDropDownOption(By by, String value) {
        List<WebElement> options = getDropDownOptions(findElement(by));
        for (WebElement element : options) {
            if (element.getAttribute("value").equalsIgnoreCase(value)) {
                return element;
            }
        }
        return NULL_WEB_ELEMENT;
    }

    public static WebElement getDropDownOption(WebElement webElement, String value) {
        List<WebElement> options = getDropDownOptions(webElement);
        for (WebElement element : options) {
            if (element.getAttribute("value").equalsIgnoreCase(value)) {
                return element;
            }
        }
        return NULL_WEB_ELEMENT;
    }


    public static void clickDropDownOption(String id, String value) {
        WebElement element = getDropDownOption(By.id(id), value);
        if (element != NULL_WEB_ELEMENT) {
            element.click();
        } else {
            LOGGER.error("Element: " + id + " not clicked");
        }
    }

    public static void clickDropDownOption(WebElement welement, String value) {
        WebElement element = getDropDownOption(welement, value);
        if (element != NULL_WEB_ELEMENT) {
            element.click();
        } else {
            LOGGER.error("Element: " + element.getText() + " not clicked");
        }
    }

    public static List<WebElement> getDropDownOptions(By by) {
        return getDropDownOptions(findElement(by));
    }

    public static List<WebElement> getDropDownOptions(String id) {
        return getDropDownOptions(findElement(id));
    }

    public static boolean isDisplayed(By by) {
        return findElement(by).isDisplayed();
    }

    public static boolean isDisplayed(String id) {
        return isDisplayed(By.id(id));
    }

    public static boolean isDisplayed(WebElement webelement) {
        return isDisplayed(webelement.getAttribute("id"));
    }

    public static boolean isSelected(By by) {
        return findElement(by).isSelected();
    }

    public static boolean isSelected(String id) {
        return isSelected(By.id(id));
    }

    public static Boolean elementExists(By by) {
        return findElement(by) != NULL_WEB_ELEMENT;
    }

    public static Boolean elementExists(String pId) {
        return elementExists(By.id(pId));
    }

    public static Boolean elementExists(WebElement webElement) {
        return elementExists(By.id(webElement.getAttribute("id")));
    }

    //Get, use and reload Driver

    public static WebDriver getCurrentDriver() {
        try {
            if (mDriver == null || mDriver.getWindowHandle() == null) {
                LOGGER.info("mDriver is null - get new webdriver");
                mDriver = BrowserFactory.getBrowser();

            }
        } catch (UnreachableBrowserException e) {
            LOGGER.info("get new webdriver, unreachableBrowserException thrown");
            mDriver = BrowserFactory.getBrowser();
        }
        return mDriver;
    }

    private static void reloadBrowser() {
        boolean retry = true;
        int attempts = 3;
        while (retry || attempts != 0) {
            try {
                getCurrentDriver();
                retry = false;
                attempts = 0;
            } catch (UnreachableBrowserException e) {
                attempts--;
            }
        }
    }

    public static void waitFor(long millis) {
        pauseSearchFor(millis);
    }

    public static void press(Keys key) {
        new Actions(getCurrentDriver()).keyDown(key).keyUp(key).perform();
    }

    public static String getPageHTML() {
        return getCurrentDriver().getPageSource();
    }


    public static void selectFrame(String pId) {
        getCurrentDriver().switchTo().frame(pId);
    }

    public static void close() {
        try {
            getCurrentDriver().quit();
            LOGGER.info("~~~~~~~~~  browser closed ~~~~~~~~~");
        } catch (UnreachableBrowserException e) {

            LOGGER.info("CANNOT QUIT BROWSER. UnreachableBrowserException thrown");
        }
    }


    public static void deleteAllCookies() {
        LOGGER.info("deleteAllCookies()");
        deleteAllCookies(null);
    }

    public static void deleteAllCookies(String url) {
        LOGGER.info("deleteAllCookies(url)");

        if (url != null) {
            LOGGER.info("URL to delete cookies: " + url);
            loadPage(url);
        }

        Set<Cookie> allCookies = getCurrentDriver().manage().getCookies();
        if (allCookies != null) {
            for (Cookie cookie : allCookies) {
                LOGGER.info("Cookie name before : " + cookie.getName() + " Cookie value: " + cookie.getValue());
            }
            getCurrentDriver().manage().deleteAllCookies();

            allCookies = getCurrentDriver().manage().getCookies();
            for (Cookie cookie : allCookies) {
                LOGGER.info("Cookie name after : " + cookie.getName() + " Cookie value: " + cookie.getValue());
            }
        } else {
            LOGGER.info("~~~~~~~~ NO COOKIES ~~~~~~~~~~");
        }
    }


    public static void loadPage(String url) {
        getCurrentDriver();
        LOGGER.info("loadPage in: " + className);
        LOGGER.info("url is: " + url);
        try {
            LOGGER.info("try to loadPage [" + url + "]");
            getCurrentDriver().get(url);
        } catch (UnreachableBrowserException e) {
            getCurrentDriver();
            try {
                getCurrentDriver().get(url);
            } catch (UnreachableBrowserException second) {
                reopenAndLoadPage(url);
            }
        }

    }

    public static void reopenAndLoadPage(String url) {
        try {
            mDriver = null;
            getCurrentDriver();
            loadPage(url);
        } catch (WebDriverException we) {
            if (we.toString().contains("Session ID may not be null")) {
                LOGGER.info("reopening failed. Session ID was null");
                throw new WebDriverException(we);
            }
        }
    }

    public static WebElement getDivByText(String divText) {
        String xPath = "//div[text()='" + divText + "']";
        return findElement(By.xpath(xPath));
    }

    public void andWaitTimeSetTo(int i) {
        WAIT_SECONDS = i;
    }

    public static void pauseSearchFor(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private static WebElement get(By by) {
        Timer timer = new Timer(10);
        while (!timer.hasExpired()) {
            try {

                reloadBrowser();
                WebElement element = mDriver.findElement(by);
                if (element == null) continue;
                if (element.isDisplayed()) {
                    return element;
                }
            } catch (NoSuchElementException e) {
                LOGGER.info("NoSuchElementException thrown, now waiting");
                waitFor(500);
            }
        }

        return NULL_WEB_ELEMENT;
    }

    private static List<WebElement> getListWithWait(By by) {
        Timer timer = new Timer(10);
        while (!timer.hasExpired()) {
            try {

                reloadBrowser();
                List<WebElement> elements = mDriver.findElements(by);
                if (elements != null) {
                    if (elements.size() != 0) {
                        return elements;
                    }
                }
            } catch (NoSuchElementException e) {
                waitFor(500);
            }
        }

        return NULL_WEB_ELEMENT_LIST;

    }

    public static WebElement waitForElementPresent(final By by) {
        return new WebDriverWait(getCurrentDriver(), 10)
                .until(new ExpectedCondition<WebElement>() {
                    public WebElement apply(@Nullable WebDriver webDriver) {
                        return findElement(by);
                    }
                });
    }

    public static String getXPath(WebElement element) {
        String jscript = "function getPathTo(node){" +
                " var stack = [];" +
                " while(node.parentNode !== null) {" +
                "  stack.unshift(node.tagName + '[id=' + node.id + ',class=' + node.className + ']');" +
                "  node = node.parentNode;" +
                " }" +
                " return stack.join('/');" +
                "}" +
                "return getPathTo(arguments[0]);";
        return (String) ((JavascriptExecutor) mDriver).executeScript(jscript, element);
    }

    /**
     * For some reason, getting children using By.CssSelector is very slow with WebElement... this is so much faster
     */
    public static List<WebElement> getChildren(WebElement element) {
        String jscript = "return arguments[0].childNodes";
        List<Object> children = (List<Object>) ((JavascriptExecutor) mDriver).executeScript("return arguments[0].childNodes", element);
        List<WebElement> elements = new ArrayList<WebElement>();
        for (Object child : children) {
            if (child instanceof WebElement) {
                elements.add((WebElement) child);
            }
        }
        return elements;
    }

    public static WebElement getParent(WebElement element) {
        return element.findElement(By.xpath(".."));
    }

    /**
     * Get a node's previous node (equivalent to DOM's "previousSibling" attribute)
     */
    public static WebElement getPreviousSibling(WebElement element) {
        Object response = ((JavascriptExecutor) mDriver).executeScript("return arguments[0].previousSibling", element);
        if (response instanceof WebElement) {
            return (WebElement) response;
        } else {
            return null;
        }
    }

    public static void executeJS(String jsCode) {
        JavascriptExecutor js = (JavascriptExecutor) mDriver;
        js.executeScript(jsCode);
    }

    public static boolean isDisplayedCheck(WebElement element) {
        int numberOfIterations = WAIT_SECONDS * 5;
        for (int i = 0; i < numberOfIterations; i++) {
            if (element.isDisplayed()) {
                return true;
            } else {
                waitFor(200);
            }
        }
        return false;
    }

    public static boolean isNotDisplayedCheck(WebElement element) {
        int numberOfIterations = WAIT_SECONDS * 5;
        //if element is deleted return true (i.e catch noSuchElementException)
        try {
            for (int i = 0; i < numberOfIterations; i++) {
                if (element.isDisplayed()) {
                    waitFor(200);
                } else {
                    return true;
                }
            }
            return false;
        } catch (NoSuchElementException e) {
            return true;
        } catch (StaleElementReferenceException e) {
            return true;
        }
    }

    public static void clickDropDownOptionByText(WebElement webelement, String text) {
        try {
            (new Select(webelement)).selectByVisibleText(text);
        } catch (NoSuchElementException nse) {
            LOGGER.error("Given option " + text + " not exists");
            throw new NoSuchElementException(text);
        }

    }

    public static Boolean elementExistsByClass(WebElement webElement) {
        try {

            return elementExists(By.className(webElement.getAttribute("class")));

        } catch (NoSuchElementException nse) {
            LOGGER.info("Element not found");
        }
        return false;
    }


}
