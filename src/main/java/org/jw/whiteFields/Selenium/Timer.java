package org.jw.whiteFields.Selenium;

public class Timer {

	private long expiryTimeMilli;

	public Timer(int timerInSeconds){
		expiryTimeMilli = currentTimeMilli() + (timerInSeconds * 1000);
	}

	public boolean hasExpired(){
		return (currentTimeMilli() > expiryTimeMilli);
	}

	private long currentTimeMilli() {
		return System.currentTimeMillis();
	}
}

