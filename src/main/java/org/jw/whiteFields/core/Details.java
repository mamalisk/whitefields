package org.jw.whiteFields.core;

public class Details {

    private String detailsDivText;

    public String getDetailsDivText() {
        return detailsDivText;
    }

    public void setDetailsDivText(String detailsDivText) {
        this.detailsDivText = detailsDivText;
    }

    public String toString(){
        return detailsDivText;
    }
}
