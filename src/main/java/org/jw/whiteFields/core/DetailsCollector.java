package org.jw.whiteFields.core;

import org.apache.commons.io.FileUtils;
import org.jw.whiteFields.Selenium.BrowserDriver;
import org.jw.whiteFields.helper.FileParser;
import org.jw.whiteFields.pages.ResultPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DetailsCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(DetailsCollector.class);

    private static ResultPage resultsPage = new ResultPage();

    public static List<Details> getDetails() throws IOException {
        File report = new File("report_ind.txt");

        if (!report.exists()) {
            report.createNewFile();
        }
        LOGGER.info("writing to file: report_ind.txt");
        List<String> allSurnames = FileParser.getSurnames();
        List<Details> result = new ArrayList<Details>();
        Integer stopAtIndex = FileParser.getStopAtIndex();
        Integer startAtIndex = FileParser.getLastIndex();
        if(startAtIndex > stopAtIndex) {
            throw new RuntimeException("Stop at less than start at index. check your files!!!");
        }
        LOGGER.info("Performing search from: " + startAtIndex + " to " + stopAtIndex);
        for (int i=startAtIndex; i<=stopAtIndex; i++) {
            String surname = allSurnames.get(i);

                LOGGER.info("Searching surname: " + surname);
                LOGGER.info("Research for [" + allSurnames.indexOf(surname) + "]");

                FileUtils.write(new File(FileParser.lastIndex), allSurnames.indexOf(surname) + "");
                FileUtils.write(new File(FileParser.lastSurname), surname);
                List<Details> detailsForSurname = getDetailsForAllPages(surname);
                result.addAll(detailsForSurname);
                ReportGenerator.toFile(result, "report.csv");
                if(detailsForSurname.size() > 0){
                    for(Details detail : detailsForSurname){
                        writeToFile(report, detail.getDetailsDivText());
                    }
                }

        }
        BrowserDriver.close();
        return result;
    }

    private static void writeToFile(File file, String data) {
        try {


            FileWriter fileWritter = new FileWriter(file.getName(), true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(data);
            bufferWritter.close();

            LOGGER.info("written to file");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Details> getDetailsForAllPages(String s) {
        List<Details> details = new ArrayList<Details>();
        LOGGER.info(s + " --> Visiting page 1");
        String currentUrl = "http://www.infobel.com/en/uk/people.aspx?QLastname=" + s + "&QCity=London";
        BrowserDriver.loadPage(currentUrl);
        List<Details> detailsToAdd = resultsPage.getDetails(currentUrl);
        if (detailsToAdd.size() > 0) {
            details.addAll(detailsToAdd);
        }

        for (int i = 1; i < 7; i++) {
            if (detailsToAdd.size() == 5) {
                LOGGER.info(s + " --> Visiting page " + i);
                int k = i * 5;
                currentUrl = "http://www.infobel.com/en/uk/people.aspx?FirstRec=" + k + "&QLastname=" + s + "&QCity=London";
                BrowserDriver.loadPage(currentUrl);
                detailsToAdd = resultsPage.getDetails(currentUrl);
                if (detailsToAdd.size() > 0) {
                    details.addAll(detailsToAdd);
                }
            } else {
                break;
            }
        }
        return details;
    }


    public static void main(String[] args) throws IOException {
        System.setProperty("browser", "firefox");
        List<Details> details = getDetails();
    }
}
