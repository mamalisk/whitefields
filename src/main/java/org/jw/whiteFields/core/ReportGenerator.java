package org.jw.whiteFields.core;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ReportGenerator {

    public static void generateDetailsReport(String filename) throws IOException {
        toFile(DetailsCollector.getDetails(),filename);
    }

    public static void toFile(List<Details> data, String filename) throws IOException {
        FileUtils.writeLines(new File(filename),data);
    }

    public static void main(String[] args) throws IOException {
        generateDetailsReport("details_report.csv");
    }
}
