package org.jw.whiteFields.helper;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileParser {

    private static final String list = "full_list.txt";
    public static final String lastIndex = "last_index.txt";
    public static final String lastSurname = "last_surname.txt";
    public static final String stopAtIndex = "stop_at_index.txt";

    public static List<String> getSurnames() throws FileNotFoundException {
        List<String> surnames = new ArrayList<String>();
        try {
            String allRows = FileUtils.readFileToString(getFile(list, true));
            surnames = Arrays.asList(allRows.split("\n"));
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        } finally {
            return surnames;
        }
    }

    public static int getSurnameIndex(String surname) throws FileNotFoundException {
        return getSurnames().indexOf(surname);
    }

    public static String getSurnameAtIndex(int index) throws FileNotFoundException {
        return getSurnames().get(index);
    }

    public static Integer getLastIndex() throws FileNotFoundException {
        Integer lastIndexToUse = 0;
        try {
            String allRows = FileUtils.readFileToString(getFile(lastIndex, true));
            lastIndexToUse = Integer.parseInt(allRows);
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        } finally {
            return lastIndexToUse;
        }
    }
    public static Integer getStopAtIndex() throws FileNotFoundException {
        Integer stopAtIndex = 0;
        try {
            String allRows = FileUtils.readFileToString(getFile(FileParser.stopAtIndex, true));
            stopAtIndex = Integer.parseInt(allRows);
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        } finally {
            return stopAtIndex;
        }
    }

    public static File getFile(String filename, boolean throwErrorOnFileNotFound) throws IOException {
        File file = new File(filename);
        if (!file.exists()) file = FileUtils.toFile(FileParser.class.getClassLoader().getResource(filename));
        if (throwErrorOnFileNotFound && (file == null || !file.exists()))
            throw new IOException("File '" + filename + "' does not exist");
        return (file.exists() ? file : null);
    }

    public static void main(String[] args) throws FileNotFoundException {
        List<String> allSurnames = getSurnames();
        System.out.println(allSurnames.size());
    }
}
